#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include <QVector>
#include <QPushButton>
#include <QLabel>
#include <QPair>
#include <QLayout>
#include <QMessageBox>
#include <QtGlobal>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include "model.h"

class View : public QWidget
{
    Q_OBJECT

public:
    View(QWidget *parent = 0);
    ~View();

private:
    QPushButton *button3;
    QPushButton *button5;
    QPushButton *button9;

    QPushButton *buttonSave;
    QPushButton *buttonLoad;

    QVBoxLayout *mainLayout;
    QHBoxLayout *menuLayout;
    QGridLayout *fieldsLayout;

    QVector<QVector<QPushButton*> > lines;
    QVector<QVector<QLabel*> > squares;

    Model *model;
    int tableSize;
    int realSize;
    QVector<QVector<QPair<int, int> > > isLine;
    int currPlayer;

private slots:
    void newGameTmp(int n);
    void newGame3();
    void newGame5();
    void newGame9();
    void newLine(int x, int y, int whose);
    void newSquare(int x1, int y1, int x2, int y2, int whose);
    void endGame(int winner);
    void click();
    void fileLoad();
    void fileSave();
    void showFields(int n);


};

#endif // VIEW_H
