#include "model.h"
#include <QMessageBox>

Model::Model()
{

}
void Model::newGame(int n)
{
    tableSize = n;
    realSize = 2 * tableSize-1;
    lines.resize(realSize);

    // first: van -e vonal, 0 ha nincs, 1 ha van
    // second: kié, ha ott alapvetően nem akarok buttont, akkor -2, ha akarok, de még üres akkor -1, egyébként
    // current player

    for (int i = 0; i < realSize; ++i)
    {
        lines[i].resize(realSize);
        for (int j = 0; j < realSize; ++j)
        {
            lines[i][j].first = 0;
            lines[i][j].second = -2;
            if (i % 2 == 0)
            {
                if (j % 2 == 1)
                {
                    lines[i][j].second = -1;
                }
            }
            else
            {
                if (j % 2 == 0)
                {
                    lines[i][j].second = -1;
                }
            }
        }
    }

    squares.resize(realSize);
    for (int i = 0; i < squares.size(); ++i)
    {
        squares[i].resize(realSize);

        for (int j = 0; j < squares[i].size(); ++j)
        {
            squares[i][j] = -1;
        }
    }

    gameStanding.resize(2);
    gameStanding[0] = 0;
    gameStanding[1] = 0;

    currentPlayer = 0;
    sumSquares = 0;
}
void Model::click(int x, int y)
{
    if (lines[x][y].first == 1)
    {
        return;
    }

    lines[x][y].first = 1;
    lines[x][y].second = currentPlayer;
    ModelNewLine(x, y, currentPlayer);

    if (!isSquare(x, y))
    {
        currentPlayer = (currentPlayer + 1) % 2;
    }

    if (isEnd())
    {
        if (gameStanding[0] == gameStanding[1])
        {
            ModelEndGame(-1);
        }

        if (gameStanding[0] > gameStanding[1])
        {
            ModelEndGame(0);
        }

        if (gameStanding[0] < gameStanding[1])
        {
            ModelEndGame(1);
        }
    }
}

bool Model::isSquare(int x, int y)
{
    int count = 0;
    if (x == 0)
    {
        if (lines[x+1][y-1].first + lines[x+2][y].first + lines[x+1][y+1].first == 3)
        {
            ModelNewSquare(x, y, x+2, y, currentPlayer);
            squares[x+1][y] = currentPlayer;
            ++sumSquares;
            ++gameStanding[currentPlayer];
            return true;
        }
        return false;
    }

    if (y == 0)
    {
        if (lines[x-1][y+1].first + lines[x][y+2].first + lines[x+1][y+1].first == 3)
        {
            ModelNewSquare(x, y, x, y+2, currentPlayer);
            squares[x][y+1] = currentPlayer;
            ++sumSquares;
            ++gameStanding[currentPlayer];
            return true;
        }
        return false;
    }

    if (x == realSize - 1)
    {
        if (lines[x-1][y-1].first + lines[x-2][y].first + lines[x-1][y+1].first == 3)
        {
            ModelNewSquare(x, y, x-2, y, currentPlayer);
            squares[x-1][y] = currentPlayer;
            ++sumSquares;
            ++gameStanding[currentPlayer];
            return true;
        }
        return false;
    }

    if (y == realSize - 1)
    {
        if (lines[x-1][y-1].first + lines[x][y-2].first + lines[x+1][y-1].first == 3)
        {
            ModelNewSquare(x, y, x, y-2, currentPlayer);
            squares[x][y-1] = currentPlayer;
            ++sumSquares;
            ++gameStanding[currentPlayer];
            return true;
        }
        return false;
    }

    if (x % 2 == 1) // álló
    {
        if (y < realSize - 2)
        {
           if (lines[x-1][y+1].first + lines[x][y+2].first + lines[x+1][y+1].first == 3)
            {
                ModelNewSquare(x, y, x, y+2, currentPlayer);
                squares[x][y+1] = currentPlayer;
                ++sumSquares;
                ++gameStanding[currentPlayer];
                ++count;
            }
        }

        if (y > 1)
        {
           if (lines[x-1][y-1].first + lines[x][y-2].first + lines[x+1][y-1].first == 3)
            {
                ModelNewSquare(x, y, x, y-2, currentPlayer);
                squares[x][y-1] = currentPlayer;
                ++sumSquares;
                ++gameStanding[currentPlayer];
                ++count;
            }
        }

        return count != 0;

    }

    if (x % 2 == 0) // fekvő
    {
        if (x < realSize - 2)
        {
            if (lines[x+1][y-1].first + lines[x+2][y].first + lines[x+1][y+1].first == 3)
            {
                ModelNewSquare(x, y, x+2, y, currentPlayer);
                squares[x+1][y] = currentPlayer;
                ++sumSquares;
                ++gameStanding[currentPlayer];
                ++count;
            }
        }

        if (x > 1)
        {
          if (lines[x-1][y-1].first + lines[x-2][y].first + lines[x-1][y+1].first == 3)
            {
                ModelNewSquare(x, y, x-2, y, currentPlayer);
                squares[x-1][y] = currentPlayer;
                ++sumSquares;
                ++gameStanding[currentPlayer];
                ++count;
            }
        }

        return count != 0;
    }

    return false;
}

bool Model::isEnd()
{
    return sumSquares == (tableSize-1)*(tableSize-1);
}

QVector<QVector<QPair<int, int> > > Model::getLines()
{
    return lines;
}

QVector<QVector<int> > Model::getSquares()
{
    return squares;
}

int Model::getCurrentPlayer()
{
    return currentPlayer;
}

QVector<int> Model::getGameStanding()
{
    return gameStanding;
}

bool Model::getIsEnd()
{
    return isEnd();
}

void Model::loadGame(QString fileName)
{
    QVector<int> saveGameData;

    int tmp = 1;

    dataAcces.loadGame(fileName, saveGameData);

    tableSize = saveGameData[0];
    realSize = 2*tableSize - 1;

    lines.resize(realSize);


    for (int i = 0; i < realSize; ++i)
    {
        for (int j = 0; j < realSize; ++j)
        {
            lines[i].push_back(qMakePair(saveGameData[tmp], saveGameData[tmp+1]));
            tmp += 2;
        }
    }

    squares.resize(realSize);

    for (int i = 0; i < realSize; ++i)
    {
        for (int j = 0; j < realSize; ++j)
        {
            squares[i].push_back(saveGameData[tmp]);
            ++tmp;
        }
    }

    gameStanding.push_back(saveGameData[tmp]);
    ++tmp;
    gameStanding.push_back(saveGameData[tmp]);
    ++tmp;
    currentPlayer = saveGameData[tmp];
    ++tmp;
    sumSquares = saveGameData[tmp];

    ModelShowFields(tableSize);
}

void Model::saveGame(QString fileName)
{
    QVector<int> saveGameData;
    saveGameData.push_back(tableSize);

    for (int i = 0; i < lines.size(); ++i)
    {
        for (int j = 0; j < lines[i].size(); ++j)
        {
            saveGameData.push_back(lines[i][j].first);
            saveGameData.push_back(lines[i][j].second);
        }
    }

    for (int i = 0; i < squares.size(); ++i)
    {
        for (int j = 0; j < squares[i].size(); ++j)
        {
            saveGameData.push_back(squares[i][j]);
        }
    }

    saveGameData.push_back(gameStanding[0]);
    saveGameData.push_back(gameStanding[1]);
    saveGameData.push_back(currentPlayer);
    saveGameData.push_back(sumSquares);

    dataAcces.saveGame(fileName, saveGameData);
}
