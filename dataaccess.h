#ifndef DATAACCESS_H
#define DATAACCESS_H

#include <QString>
#include <QVector>

class DataAccess
{
public:
    DataAccess();

    void loadGame(QString fileName, QVector<int> &saveGameData);
    void saveGame(QString fileName, QVector<int> &saveGameData);
};

#endif // DATAACCESS_H
