#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QVector>
#include <QPair>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include "dataaccess.h"

class Model : public QObject
{
    Q_OBJECT
public:
    Model();
    void newGame(int n);
    void click(int x, int y);
    QVector<QVector<QPair<int, int> > > getLines();
    QVector<QVector<int> > getSquares();
    int getCurrentPlayer();
    QVector<int> getGameStanding();
    bool getIsEnd();
    void loadGame(QString fileName);
    void saveGame(QString fileName);

private:
    QVector<QVector<QPair<int, int> > > lines;
    QVector<QVector<int> > squares;
    int currentPlayer;
    int tableSize;
    bool isEnd();
    bool isSquare(int x, int y);
    int realSize;
    int sumSquares;
    QVector<int> gameStanding;

    DataAccess dataAcces;

signals:
    void ModelNewLine(int x, int y, int whose);
    void ModelNewSquare(int x1, int y1, int x2, int y2, int whose);
    void ModelEndGame(int winner);
    void ModelShowFields(int n);

};

#endif // MODEL_H
