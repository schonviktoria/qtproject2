#include "test.h"

void Test::initTestCase()
{
    model = new Model();
}

void Test::cleanupTestCase()
{
    delete model;
}

void Test::testNewGame()
{
    model->newGame(2);
    lines = model->getLines();
    squares = model->getSquares();
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            if (i % 2 == 0)
            {
                if (j % 2 == 1)
                {
                    QCOMPARE(lines[i][j].first, 0);
                    QCOMPARE(lines[i][j].second, -1);
                }
            }
            else
            {
                if (j % 2 == 0)
                {
                    QCOMPARE(lines[i][j].first, 0);
                    QCOMPARE(lines[i][j].second, -1);
                }
            }

        }
    }

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            QCOMPARE(squares[i][j], -1);
        }
    }

    gameStanding = model->getGameStanding();
    QCOMPARE(gameStanding[0], 0);
    QCOMPARE(gameStanding[1], 0);
    QCOMPARE(model->getCurrentPlayer(), 0);
    QCOMPARE(model->getIsEnd(), false);

}

void Test::testClick()
{
    model->newGame(2);
    // játékos váltás
    model->click(0,1);
    QCOMPARE(model->getCurrentPlayer(), 1);
    model->click(2, 1);
    QCOMPARE(model->getCurrentPlayer(), 0);
}

void Test::testEndGame()
{
    model->newGame(2);
    model->click(0,1);
    QCOMPARE(model->getCurrentPlayer(), 1);
    model->click(2, 1);
    model->click(1, 0);
    model->click(1, 2);
    gameStanding = model->getGameStanding();
    QCOMPARE(gameStanding[1], 1);
    QCOMPARE(gameStanding[0], 0);
    QCOMPARE(model->getIsEnd(), true);
}
