#include "view.h"

View::View(QWidget *parent)
    : QWidget(parent)
{
    button3 = new QPushButton("3x3", this);
    button5 = new QPushButton("5x5", this);
    button9 = new QPushButton("9x9", this);

    buttonSave = new QPushButton("Save", this);
    buttonLoad = new QPushButton("Load", this);

    menuLayout = new QHBoxLayout;
    menuLayout->addWidget(button3);
    menuLayout->addWidget(button5);
    menuLayout->addWidget(button9);
    menuLayout->addWidget(buttonSave);
    menuLayout->addWidget(buttonLoad);

    fieldsLayout = new QGridLayout;
    fieldsLayout->setSpacing(0);

    mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(menuLayout);
    mainLayout->addLayout(fieldsLayout);

    connect(button3, SIGNAL(clicked(bool)), this, SLOT(newGame3()));
    connect(button5, SIGNAL(clicked(bool)), this, SLOT(newGame5()));
    connect(button9, SIGNAL(clicked(bool)), this, SLOT(newGame9()));   

    connect(buttonLoad, SIGNAL(clicked(bool)), this, SLOT(fileLoad()));
    connect(buttonSave, SIGNAL(clicked(bool)), this, SLOT(fileSave()));

    model = 0;
}

void View::newGameTmp(int n)
{
    setFixedSize(n * 60 + 50, n * 60 + 20);
    delete model;
    tableSize = n;
    realSize = 2 * tableSize-1;

    for (int i = 0; i < lines.size(); ++i)
    {
        for (int j = 0; j < lines[i].size(); ++j)
        {
            fieldsLayout->removeWidget(lines[i][j]);
            delete lines[i][j];
        }
    }

    for (int i = 0; i < squares.size(); ++i)
    {
        for (int j = 0; j < squares.size(); ++j)
        {
            fieldsLayout->removeWidget(squares[i][j]);
            delete squares[i][j];
        }
    }

    model = new Model();
    model->newGame(n);
    isLine = model->getLines();

    squares.resize(realSize);
    lines.resize(realSize);

    for (int i = 0; i < realSize; ++i)
    {
        lines[i].resize(realSize);
        for (int j = 0; j < realSize; ++j)
        {
            lines[i][j] = NULL;
        }
    }

    for (int i = 0; i < realSize; ++i)
    {
        squares[i].resize(realSize);
        for (int j = 0; j < realSize; ++j)
        {
            squares[i][j] = new QLabel(this);
            fieldsLayout->setSpacing(3);
            fieldsLayout->addWidget(squares[i][j], i, j);
        }
    }

    for (int i = 0; i < realSize; ++i)
    {
        for (int j = 0; j < realSize; ++j)
        {
            if (isLine[i][j].second == -1)
            {
                lines[i][j] = new QPushButton(this);

                if (i % 2 == 0)
                {
                    lines[i][j]->setFixedSize(40, 15);
                }
                else
                {
                    lines[i][j]->setFixedSize(15, 40);
                }

                fieldsLayout->addWidget(lines[i][j], i, j);
                connect(lines[i][j], SIGNAL(clicked()), this, SLOT(click()));
            }
        }
    }

    connect(model, SIGNAL(ModelNewLine(int,int,int)), this, SLOT(newLine(int,int,int)));

    connect(model, SIGNAL(ModelNewSquare(int,int,int,int,int)), this, SLOT(newSquare(int,int,int,int,int)));

    connect(model, SIGNAL(ModelEndGame(int)), this, SLOT(endGame(int)));
}

void View::newGame3()
{
    newGameTmp(3);
}

void View::newGame5()
{
    newGameTmp(5);
}

void View::newGame9()
{
    newGameTmp(9);
}

void View::newLine(int x, int y, int whose)
{
    currPlayer = model->getCurrentPlayer();
    if (whose == 0)
    {
        lines[x][y]->setStyleSheet("background-color: blue");
    }

    if (whose == 1)
    {
        lines[x][y]->setStyleSheet("background-color: red");
    }
}

void View::newSquare(int x1, int y1, int x2, int y2, int whose)
{
    QString color = whose == 0 ? "blue" : "red";
    if (x1 == x2)
    {
        squares[x1][qMax(y1, y2)-1]->setStyleSheet("QLabel {background-color: " + color + "; }");
    }

    if (y1 == y2)
    {
        squares[qMax(x1, x2)-1][y1]->setStyleSheet("QLabel {background-color: " + color + "; }");
    }
}

void View::click()
{
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    int ind;
    ind = fieldsLayout->indexOf(sender);
    int x, y, tmp;
    fieldsLayout->getItemPosition(ind, &x, &y, &tmp, &tmp);
    model->click(x, y);
}

void View::endGame(int winner)
{
    QMessageBox msg(this);

    if (winner == 0)
    {
       msg.setText("The end. The winner is blue.");
       msg.exec();
    }
    if (winner == 1)
    {
        msg.setText("The end. The winner is red.");
        msg.exec();
    }
    if (winner == -1)
    {
        msg.setText("The end. The game is draw.");
        msg.exec();
    }
}

void View::fileLoad()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open game"), QDir::currentPath(), tr("Text files(*.txt)"));
    if (fileName.isNull())
        return;
    delete model;
    model = new Model();
    connect(model, SIGNAL(ModelShowFields(int)), this, SLOT(showFields(int)));
    model->loadGame(fileName);
}

void View::fileSave()
{
    if (model == 0)
    {
        QMessageBox msg;
        msg.setText("The model is empty.");
        msg.exec();
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save game"), QDir::currentPath(), tr("Text files(*.txt)"));
    model->saveGame(fileName);
}

void View::showFields(int n)
{
   connect(model, SIGNAL(ModelNewLine(int,int,int)), this, SLOT(newLine(int,int,int)));
   connect(model, SIGNAL(ModelNewSquare(int,int,int,int,int)), this, SLOT(newSquare(int,int,int,int,int)));
   connect(model, SIGNAL(ModelEndGame(int)), this, SLOT(endGame(int)));

   QVector<QVector<QPair<int, int> > > v_lines = model->getLines();
   QVector<QVector<int> > v_squares = model->getSquares();

   setFixedSize(n * 60 + 50, n * 60 + 20);

   tableSize = n;
   realSize = 2 * tableSize-1;

   for (int i = 0; i < lines.size(); ++i)
   {
       for (int j = 0; j < lines[i].size(); ++j)
       {
           fieldsLayout->removeWidget(lines[i][j]);
           delete lines[i][j];
       }
   }

   for (int i = 0; i < squares.size(); ++i)
   {
       for (int j = 0; j < squares.size(); ++j)
       {
           fieldsLayout->removeWidget(squares[i][j]);
           delete squares[i][j];
       }
   }


    squares.resize(realSize);
    lines.resize(realSize);

    for (int i = 0; i < realSize; ++i)
    {
        lines[i].resize(realSize);
        squares[i].resize(realSize);
        for (int j = 0; j < realSize; ++j)
        {
            lines[i][j] = NULL;
            squares[i][j] = NULL;
        }
    }

    for (int i = 0; i < realSize; ++i)
    {
        squares[i].resize(realSize);
        for (int j = 0; j < realSize; ++j)
        {
            squares[i][j] = new QLabel(this);
            fieldsLayout->setSpacing(3);
            fieldsLayout->addWidget(squares[i][j], i, j);
        }
    }

    for (int i = 0; i < lines.size(); ++i)
    {
        for (int j = 0; j < lines.size(); ++j)
        {
            if (v_lines[i][j].second == -1 || v_lines[i][j].second == 0 || v_lines[i][j].second == 1)
            {
                lines[i][j] = new QPushButton(this);
                if (i % 2 == 0)
                {
                    lines[i][j]->setFixedSize(40, 15);
                }
                else
                {
                    lines[i][j]->setFixedSize(15, 40);
                }

                fieldsLayout->addWidget(lines[i][j], i, j);
                connect(lines[i][j], SIGNAL(clicked()), this, SLOT(click()));
            }
            if (v_lines[i][j].second == 0)
            {
                lines[i][j]->setStyleSheet("background-color: blue");
            }
            if (v_lines[i][j].second == 1)
            {
                lines[i][j]->setStyleSheet("background-color: red");
            }
        }
    }

    for (int i = 0; i < v_squares.size(); ++i)
    {
        for (int j = 0; j < v_squares[i].size(); ++j)
        {
            if (v_squares[i][j] != -1)
            {
                QString color = v_squares[i][j] == 0 ? "blue" : "red";
                squares[i][j]->setStyleSheet("QLabel {background-color: " + color + "; }");
            }

        }
    }
}


View::~View()
{

}
