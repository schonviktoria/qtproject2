#ifndef TEST_H
#define TEST_H

#include <QtTest>
#include "model.h"

class Test : public QObject
{
    Q_OBJECT

private:
    Model* model;
    QVector<QVector<int> > squares;
    QVector<QVector<QPair<int, int>  > > lines;
    QVector<int> gameStanding;
private slots:
    void initTestCase();
    void cleanupTestCase();
    void testNewGame();
    void testClick();
    void testEndGame();
};

#endif // TEST_H
