#include "view.h"
#include <QApplication>
#include "test.h"

int main(int argc, char *argv[])
{
    Test mtest;
    QTest::qExec(&mtest, argc, argv);

    QApplication a(argc, argv);
    View w;
    w.show();

    return a.exec();
}
