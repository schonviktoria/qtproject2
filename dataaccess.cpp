#include "dataaccess.h"
#include <QFile>
#include <QTextStream>

DataAccess::DataAccess()
{
}

void DataAccess::loadGame(QString fileName, QVector<int> &saveGameData)
{
    int tmp;
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);

    while (!in.atEnd())
    {
        in >> tmp;
        saveGameData.push_back(tmp);
    }

    file.close();
}

void DataAccess::saveGame(QString fileName, QVector<int> &saveGameData)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);

    for (int i = 0; i < saveGameData.size(); ++i)
    {
        out << saveGameData[i] << endl;
    }

    file.close();

}
